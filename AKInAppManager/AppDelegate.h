//
//  AppDelegate.h
//  AKInAppManager
//
//  Created by Alex Kuzovkov on 21.09.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

