//
//  ViewController.h
//  AKInAppManager
//
//  Created by Alex Kuzovkov on 21.09.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKInAppManager.h"

@interface ViewController : UIViewController <AKInAppManagerDelegate>

- (IBAction)buy:(id)sender;
- (IBAction)restore:(id)sender;

@end

