//
//  main.m
//  AKInAppManager
//
//  Created by Alex Kuzovkov on 21.09.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
