//
//  ViewController.m
//  AKInAppManager
//
//  Created by Alex Kuzovkov on 21.09.17.
//  Copyright © 2017 Aleksey Kuzovkov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    AKInAppManager *manager;
    AKInAppProduct * proVersionProduct;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Initialise InAppManager
    manager = [AKInAppManager sharedManager];
    
    //Set this to true for development
    [manager setTestMode:true];
    
    //Set delegate for InAppManager
    [manager setDelegate:self];
    
    //Set true for InAppManager to automaticaly block view
    [manager setViewBlockEnabled:true];
    
    //Set message for block view
    [manager setViewBlockMessage:@"Purchase in progress"];
    
    //Set view auto unblock when purchase completed
    [manager setViewAutoUnBlockEnabled:true];
    
    /*
    If view auto unblock is disabled you can unlock in manualy with:
    [manager unblock];
     */
    
    //Reference to product
    proVersionProduct = [manager getProductWithIdentifier:@"com.alexeykuzovkov.productidentifier.pro"];
   
    //Check if product purchased
    //proVersionProduct.purchased;
}

#pragma mark Actions
- (IBAction)buy:(id)sender {
    //Init purchase process
    [manager purchaseProduct:proVersionProduct];
}

- (IBAction)restore:(id)sender {
    //Init restore process
    [manager restoreProducts];
}

#pragma mark InAppDelegate

-(void)AKInAppDidPurchaseProduct:(AKInAppProduct*)product withReceipt:(NSString *)receipt {
    //InAppProduct purchased
}


-(void)AKInAppDidRestoreProduct:(AKInAppProduct*)product {
    //InAppProduct restored
}

-(void)AKInAppPaymentFailedWithCode:(PaymentFailCode)failCode {
    switch (failCode) {
        case ParentalRestrictions:
            //Parental restrictions
            break;
            
        case NoProduct:
            //Product not found
            break;
            
        case RestoreFail:
            //Restore fails
            break;
            
        case ApprovalWait:
            //Parental approval needed
            break;
            
        case Canceled:
            //Payment canceled
            break;
            
        case PurchaseFail:
            //Purchase failed
            break;
        default:
            //default
            break;
    }
}


@end
