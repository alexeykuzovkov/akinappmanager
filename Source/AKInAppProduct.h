//
//  InAppProduct.h
//  RealNewGuitar
//
//  Created by Alex Kuzovkov on 03.04.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AKInAppProduct : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic) BOOL purchased;
@end
