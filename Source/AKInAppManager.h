//
//  InAppManager.h
//  RealNewGuitar
//
//  Created by Alex Kuzovkov on 03.04.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import <UIKit/UIKit.h>
#import "AKInAppProduct.h"

typedef enum {
    ParentalRestrictions,
    NoProduct,
    RestoreFail,
    ApprovalWait,
    Canceled,
    PurchaseFail
} PaymentFailCode ;

@protocol AKInAppManagerDelegate <NSObject>

-(void)AKInAppPaymentFailedWithCode:(PaymentFailCode)failCode;
-(void)AKInAppDidRestoreProduct:(AKInAppProduct*)product;
-(void)AKInAppDidPurchaseProduct:(AKInAppProduct*)product withReceipt:(NSString*)receipt;

@end

@interface AKInAppManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (nonatomic, assign) id <AKInAppManagerDelegate> delegate;

@property (nonatomic, strong, readonly) NSArray *products;
@property (nonatomic)                   BOOL    viewBlockEnabled;
@property (nonatomic)                   BOOL    viewAutoUnBlockEnabled;
@property (nonatomic)                   BOOL    testMode;

@property (nonatomic, strong)           NSString *viewBlockMessage;

+(id)sharedManager;

-(AKInAppProduct*)getProductWithIdentifier:(NSString*)identifier;

-(void)unblock;

-(void)purchaseProduct:(AKInAppProduct*)product;
-(void)restoreProducts;

@end
