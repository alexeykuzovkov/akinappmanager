//
//  InAppManager.m
//  RealNewGuitar
//
//  Created by Alex Kuzovkov on 03.04.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

#import "AKInAppManager.h"

@interface AKInAppManager() {
    UIView *connectingAppStoreBlockView;
}

@end
@implementation AKInAppManager

#pragma mark initialiserMethods
+ (id)sharedManager {
    static AKInAppManager *sharedManager = nil;
    @synchronized(self) {
        if (sharedManager == nil) {
            sharedManager = [[self alloc] init];
        }
    }
    return sharedManager;
}

-(id)init {
    if (self == [super init]) {
        [self bindTransactionObserver];
        
        _products = [[NSArray alloc] init];
        _viewBlockEnabled = true;
        _viewAutoUnBlockEnabled = true;
        _testMode = false;
    }
    return self;
}

-(void)bindTransactionObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    for (SKPaymentTransaction *tr in [[SKPaymentQueue defaultQueue] transactions]) {
        [[SKPaymentQueue defaultQueue] finishTransaction:tr];
    }
}

#pragma mark ViewBlock

-(void)createBlockView {
    UIWindow *currentWindow = [[[UIApplication sharedApplication] delegate] window];
    
    connectingAppStoreBlockView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, currentWindow.frame.size.width, currentWindow.frame.size.height)];
    [connectingAppStoreBlockView setHidden:false];
    [connectingAppStoreBlockView setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
    UILabel *connectingLabel = [[UILabel alloc] initWithFrame:connectingAppStoreBlockView.frame];
    [connectingLabel setText:_viewBlockMessage];
    [connectingLabel setTextColor:[UIColor whiteColor]];
    [connectingLabel setTextAlignment:NSTextAlignmentCenter];
    [connectingAppStoreBlockView addSubview:connectingLabel];
    [currentWindow addSubview:connectingAppStoreBlockView];
}
-(void)blockView {
    if (_viewBlockEnabled) {
        [self createBlockView];
        [connectingAppStoreBlockView setHidden:false];
    }
}
-(void)unblockView {
    if (_viewAutoUnBlockEnabled) {
        [self unblock];
    }
}

-(void)unblock {
    if (_viewBlockEnabled) {
        [connectingAppStoreBlockView setHidden:true];
        [connectingAppStoreBlockView removeFromSuperview];
    }
}

#pragma mark InAppMethods
-(AKInAppProduct*)addProductWithIdentifier:(NSString*)identifier {
    AKInAppProduct *product = [[AKInAppProduct alloc] init];
    [product setIdentifier:identifier];
    
    for (AKInAppProduct *_product in _products) {
        if([_product isEqual:product]) {
            return _product;
        }
    }
    
    NSMutableArray *productsMutable = [_products mutableCopy];
    [productsMutable addObject:product];
    _products = [NSArray arrayWithArray:productsMutable];
    
    return product;
}

-(AKInAppProduct*)getProductWithIdentifier:(NSString*)identifier {
    for (AKInAppProduct *_product in _products) {
        if([_product.identifier isEqualToString:identifier]) {
            return _product;
        }
    }
    return [self addProductWithIdentifier:identifier];
}

-(void)purchaseProduct:(AKInAppProduct*)product {
    if([SKPaymentQueue canMakePayments]){
        NSLog(@"User can make payments");
        [self blockView];
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithObject:product.identifier]];
        productsRequest.delegate = self;
        [productsRequest start];
        
    }
    else{
        NSLog(@"User cannot make payments due to parental controls");
        [_delegate AKInAppPaymentFailedWithCode:ParentalRestrictions];
    }
}
-(void)restoreProducts {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    [self blockView];
}

- (void)purchase:(SKProduct *)product{
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    if (_testMode) {
        payment.simulatesAskToBuyInSandbox = true;
    }
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}
#pragma mark InAppDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    SKProduct *validProduct = nil;
    int count = (int)[response.products count];
    if(count > 0){
        validProduct = [response.products objectAtIndex:0];
        NSLog(@"Products Available!");
        [self purchase:validProduct];
    }
    else if(!validProduct){
        [self unblockView];
        NSLog(@"No products available");
        [_delegate AKInAppPaymentFailedWithCode:NoProduct];
    }
}

- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    int restoredCount = 0;
    for(SKPaymentTransaction *transaction in queue.transactions){
        if(transaction.transactionState == SKPaymentTransactionStateRestored){
            //called when the user successfully restores a purchase
            NSLog(@"Transaction state -> Restored");
            
            //if you have more than one in-app purchase product,
            //you restore the correct product for the identifier.
            //For example, you could use
            //if(productID == kRemoveAdsProductIdentifier)
            //to get the product identifier for the
            //restored purchases, you can use
            //
            NSString *productID = transaction.payment.productIdentifier;
            AKInAppProduct *product = [self getProductWithIdentifier:productID];
            [product setPurchased:TRUE];
            [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
            [_delegate AKInAppDidRestoreProduct:product];
            restoredCount++;
        }
    }
    
    if(restoredCount==0) {
        [_delegate AKInAppPaymentFailedWithCode:RestoreFail];
    }
    
    [self unblockView];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    NSLog(@"updated transactions");
    for(SKPaymentTransaction *transaction in transactions){
        AKInAppProduct *product = [self getProductWithIdentifier:transaction.payment.productIdentifier];
        
        switch(transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing: NSLog(@"Transaction state -> Purchasing");
                //called when the user is in the process of purchasing, do not add any of your own code here.
                NSLog(@"purchasing");
                break;
            case SKPaymentTransactionStatePurchased:{
                NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
                NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
                NSLog(@"%@",[receipt base64EncodedStringWithOptions:0]);
                [_delegate AKInAppDidPurchaseProduct:product withReceipt:[receipt base64EncodedStringWithOptions:0]];
                [self unblockView];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                [product setPurchased:TRUE];
                NSLog(@"Transaction state -> Purchased");
                
                break;
            }
            case SKPaymentTransactionStateDeferred: {
                [self unblockView];
                [_delegate AKInAppPaymentFailedWithCode:ApprovalWait];
                break;
            }
            case SKPaymentTransactionStateRestored: {
                NSLog(@"Transaction state -> Restored %@", product.identifier);
                //add the same code as you did from SKPaymentTransactionStatePurchased here
                [_delegate AKInAppDidRestoreProduct:product];
                [product setPurchased:TRUE];
                [self unblockView];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
            case SKPaymentTransactionStateFailed: {
                //called when the transaction does not finish
                
                if(transaction.error.code == SKErrorPaymentCancelled){
                    NSLog(@"Transaction state -> Cancelled");
                    [_delegate AKInAppPaymentFailedWithCode:Canceled];
                }
                else {
                    [_delegate AKInAppPaymentFailedWithCode:PurchaseFail];
                }
                [self unblockView];
                [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                break;
            }
        }
    }
}

@end
