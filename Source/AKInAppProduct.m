//
//  InAppProduct.m
//  RealNewGuitar
//
//  Created by Alex Kuzovkov on 03.04.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

#import "AKInAppProduct.h"

@implementation AKInAppProduct
@synthesize purchased = _purchased;
- (BOOL)isEqual:(id)object {
    AKInAppProduct *_object = (AKInAppProduct*)object;
    return ([self.identifier isEqualToString:_object.identifier]);
}
-(void)setIdentifier:(NSString *)identifier {
    _identifier = identifier;
    [self setPurchased:[self purchased]];
}
-(void)setPurchased:(BOOL)purchased {
    _purchased = purchased;
    [[NSUserDefaults standardUserDefaults] setBool:_purchased forKey:[NSString stringWithFormat:@"InAppProduct-%@",_identifier]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(BOOL)purchased {
    return [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"InAppProduct-%@",_identifier]];
}
@end
