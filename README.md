# AKInAppManager � Simple In App Manager for iOS #

### About ###

Simple In-app purchase manager with view auto blocking and buit-in transaction observer for consumable and non-consumable in app purchases

### How to import ###

* Download zip
* Import all contents from folder named Source to your project
* Import StoreKit framework

### How to use ###

* Import AKInAppManager.h
```objective-c
#import "AKInAppManager.h"
```

* Your view controller should conform to AKInAppManagerDelegate protocol
```objective-c
@interface ViewController : UIViewController <AKInAppManagerDelegate>
```

* Initialise InAppManager

```objective-c
AKInAppManager *manager = [AKInAppManager sharedManager];
//Set test mode to true for sandbox mode
[manager setTestMode:true];
//Set delegate to self
[manager setDelegate:self];
//Set true for InAppManager to automaticaly block view
[manager setViewBlockEnabled:true];
//Set message for block view
[manager setViewBlockMessage:@"Purchase in progress"];
//Set view auto unblock when purchase completed
[manager setViewAutoUnBlockEnabled:true];
```
   
* If view auto unblock is disabled you can unlock in manualy with:
```objective-c
[manager unblock];
```
  
* Reference to your product
```objective-c
AKInAppProduct *proVersionProduct = [manager getProductWithIdentifier:@"com.alexeykuzovkov.productidentifier.pro"];
```
   
* Check if product purchased
```objective-c
proVersionProduct.purchased;
```

* Make Purchase
```objective-c
[manager purchaseProduct:proVersionProduct];
```
* Restore Purchases
```objective-c
[manager restoreProducts];
```

* Delegate methods
```objective-c
-(void)AKInAppDidPurchaseProduct:(AKInAppProduct*)product withReceipt:(NSString *)receipt {
    //InAppProduct purchased
}


-(void)AKInAppDidRestoreProduct:(AKInAppProduct*)product {
    //InAppProduct restored
}

-(void)AKInAppPaymentFailedWithCode:(PaymentFailCode)failCode {
    switch (failCode) {
        case ParentalRestrictions:
            //Parental restrictions
            break;
            
        case NoProduct:
            //Product not found
            break;
            
        case RestoreFail:
            //Restore fails
            break;
            
        case ApprovalWait:
            //Parental approval needed
            break;
            
        case Canceled:
            //Payment canceled
            break;
            
        case PurchaseFail:
            //Purchase failed
            break;
        default:
            //default
            break;
    }
}
```






